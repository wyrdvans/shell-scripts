#!/usr/bin/env zsh

SERVERS=(rs-web1 rs-web2 rs-web4 rs-web3 rs-db1 rs-db2 rs-redis-01 srv-prd-db3)

for server in $SERVERS; do
    echo "== $server"
    ssh $server grep UID_MIN /etc/login.defs
    ssh $server cat /etc/passwd | awk -F: '{if ($3 >= 500) { print $1 ":" $3 } }' > $server.txt
done

cat *.txt | cut -d: -f 1 | sort | uniq -c
