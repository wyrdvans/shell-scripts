#!/usr/bin/env zsh

echo "= Checking for $1 across production rackspace servers"

SERVERS=(rs-web1 rs-web2 rs-web4 rs-web3 rs-db1 rs-db2 rs-redis-01 srv-prd-db3)

for server in $SERVERS; do
    echo "== $server"
    if ssh $server.pfdmain2k.pri grep $1 /etc/passwd; then
        echo "== $server : checking cron"
        ssh $server.pfdmain2k.pri sudo crontab -u $1 -l
        echo "== $server : checking files in /etc"
        ssh $server.pfdmain2k.pri sudo grep -r $1 /etc
        echo "== $server : finding files owned by $1"
        ssh $server.pfdmain2k.pri sudo find / ! -path "/proc/\*" ! -path "/var/www/html/\*" ! -path "/mnt/\*" ! -path "/data/\*" -user $1 -print
        echo "== $server : checking for mail"
        ssh $server.pfdmain2k.pri sudo mail -u $1
        echo "== $server : last logged in"
        ssh $server.pfdmain2k.pri sudo last $1
    fi
done

#Remove user
#userdel -r $1
